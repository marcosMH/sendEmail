﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using sendEmail.Dto;
using sendEmail.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sendEmail.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SendEmailController : ControllerBase
    {
        #region Campos
        private readonly SendEmail _sendEmail;
        #endregion

        #region Constructor 
        public SendEmailController(SendEmail sendEmail)
        {
            _sendEmail = sendEmail;
        }
        #endregion

        #region Metodos
        [HttpPost]
        public async Task<string> Post (bodyEmail body)
        {
            return await _sendEmail.sendEmailLogic(body);
        }
        #endregion
    }
}
