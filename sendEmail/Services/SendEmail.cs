﻿using sendEmail.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sendEmail.Services
{
    public interface SendEmail
    {
        Task<string> sendEmailLogic(bodyEmail body);
    }
}
