﻿using sendEmail.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace sendEmail.Services
{
    public class SendEmailImplementacion : SendEmail
    {
        public async Task<string> sendEmailLogic(bodyEmail body)
        {
            var json = Task.Run(() => sendEmail(body));
            string result = await json;
            return result;
        }

        private string sendEmail(bodyEmail body)
        {
            try
            {
                var html = this.plantilla(body);
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("marcos_13_m_h@hotmail.com", "notificacion de portafolios");
                mail.To.Add(new MailAddress("marcos_13_m_h@hotmail.com"));
                mail.Subject = body.subject;
                mail.IsBodyHtml = true;
                mail.Body = body.message;
                
                SmtpClient smtp = new SmtpClient("smtp.office365.com", 587);
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("soy un correo", "soy una contraseña");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = true;
                smtp.Send(mail);
                return "mensaje enviado";
            }catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        private string plantilla(bodyEmail body)
        {
            var plantillaHTML = $"<!DOCTYPE html><head><meta charset='UTF-8'><meta name='viewport' content='width=device-width,initial-scale=1'><link href='https://fonts.googleapis.com/css2?family=Roboto&display=swap' rel='stylesheet'></head><body><center style='font-family:Roboto,sans-serif;font-size:16px'><div style='margin:30px auto;width:500px;border:1px solid #ddd;padding:15px 35px;box-shadow:5px 5px 15px;border-radius:5px'><h2 style='border-bottom:2px solid #ddd;padding-bottom:10px'>¡Notificación de uso del portafolios!</h2><div style='margin-top:10px;text-align:left'><p>Marcos nuevo mensaje</p><p>Informacion: <b>{body.name}</b> <b>{body.correo}</b></p><p>Mensaje: {body.message}.</p></div><div style='font-size:12px;color:#a09898;margin-top:30px'>Este email es generado automáticamente por dev marcos.</div></div></center></body>;";
            return plantillaHTML;
        }
    }
}
