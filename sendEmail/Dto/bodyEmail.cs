﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sendEmail.Dto
{
    public class bodyEmail
    {
        public string name { get; set; }
        public string correo { get; set; }
        public string subject { get; set; }
        public string message { get; set; }
    }
}
